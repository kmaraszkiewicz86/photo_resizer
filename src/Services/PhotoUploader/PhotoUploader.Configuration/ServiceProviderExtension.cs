﻿using System.Reflection;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using PhotoUploader.Utils.Configurations;
using StackExchange.Redis;

namespace PhotoUploader.Configuration
{
    public static class ServiceProviderExtension
    {
        public static IServiceCollection ConfigureServices(this IServiceCollection services)
        {


            return services;
        }

        public static IServiceCollection ConfigureInfrastrucure(this IServiceCollection services, RedisConfiguration redisConfiguration)
        {
            services.AddSingleton<IDatabase>(ConfigureRedis(redisConfiguration));

            return services;
        }

        public static IServiceCollection ConfigureAutoMapper(this IServiceCollection services, params Assembly[] assemblies)
        {
            services.AddAutoMapper(assemblies);

            return services;
        }

        public static IServiceCollection ConfigureMediator(this IServiceCollection services, params Assembly[] assemblies)
        {

            services.AddMediatR(assemblies);

            return services;
        }

        private static IDatabase ConfigureRedis(RedisConfiguration redisConfiguration)
        {
            var redis = ConnectionMultiplexer.Connect(
            new ConfigurationOptions
            {
                EndPoints = { redisConfiguration.Endpoint }
            });

            return redis.GetDatabase();
        }
    }
}
