﻿namespace PhotoUploader.Domain.Models
{
    public record ImageRequest(string Name, byte[] ImageContent, string Description);
}