﻿namespace PhotoUploader.Domain.Models
{
    public record ImageModel(string Name, string Path, string Description);
}