﻿using PhotoUploader.Domain.Models;
using PhotoUploader.Utils.Exceptions;

namespace PhotoUploader.Domain.Aggregates
{
    public class ImageAggregate
    {
        private ImageModel _image;

        public ImageAggregate(ImageModel image)
        {
            _image = image;
        }
    }
}
