﻿namespace PhotoUploader.Domain.Repository
{
    public interface IImageRedisRepository
    {
        public void SaveImageMetadata();
    }
}
