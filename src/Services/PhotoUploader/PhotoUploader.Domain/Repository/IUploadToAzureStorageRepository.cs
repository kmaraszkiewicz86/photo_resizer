﻿namespace PhotoUploader.Domain.Repository
{
    public interface IUploadToAzureStorageRepository
    {
        public string UploadBaseImage(byte[] imageBytes);
    }
}
