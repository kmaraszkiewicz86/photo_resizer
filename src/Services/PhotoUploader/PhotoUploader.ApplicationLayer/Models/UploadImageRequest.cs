﻿using Microsoft.AspNetCore.Http;

namespace PhotoUploader.Api.Models
{
    public record UploadImageRequest(string Name, IFormFile UplaodedImage, string Description);
}
