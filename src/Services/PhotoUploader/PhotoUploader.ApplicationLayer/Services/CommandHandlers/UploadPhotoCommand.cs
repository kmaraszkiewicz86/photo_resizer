﻿using MediatR;
using Microsoft.AspNetCore.Http;

namespace PhotoUploader.ApplicationLayer.Services.CommandHandlers
{
    public class UploadPhotoCommand : IRequest
    {
        public string Name { get; }
        public IFormFile UplaodedImage { get; }
        public string Description { get; }

        public UploadPhotoCommand(string name, IFormFile uplaodedImage, string description)
        {
            Name = name;
            UplaodedImage = uplaodedImage;
            Description = description;
        }
    }
}
