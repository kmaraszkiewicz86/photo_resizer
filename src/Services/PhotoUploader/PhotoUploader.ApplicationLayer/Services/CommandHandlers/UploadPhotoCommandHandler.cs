﻿using MediatR;

namespace PhotoUploader.ApplicationLayer.Services.CommandHandlers
{
    public class UploadPhotoCommandHandler : IRequestHandler<UploadPhotoCommand, Unit>
    {
        public Task<Unit> Handle(UploadPhotoCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(Unit.Value);
        }
    }
}
