﻿using FluentValidation;
using PhotoUploader.Api.Models;

namespace PhotoUploader.ApplicationLayer.Validators
{
    public class UploadImageRequestValidator : AbstractValidator<UploadImageRequest>
    {
        public UploadImageRequestValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty();

            RuleFor(x => x.Description)
                .NotEmpty();

            RuleFor(x => x.UplaodedImage)
                .NotEmpty();
        }
    }
}
