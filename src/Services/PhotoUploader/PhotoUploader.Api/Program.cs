using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using PhotoUploader.ApplicationLayer.Services.CommandHandlers;
using PhotoUploader.Configuration;
using PhotoUploader.Utils.Configurations;

namespace PhotoUploader.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddCors(options =>
            {
                options.AddDefaultPolicy(policy => policy.AllowAnyHeader()
                                                            .AllowAnyHeader()
                                                            .AllowAnyOrigin());
            });

            builder.Services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            var redisConfiguration = builder.Configuration.GetSection("Redis").Get<RedisConfiguration>();

            builder.Services
                .ConfigureAutoMapper(Assembly.GetAssembly(typeof(Program)))
                .ConfigureMediator(Assembly.GetAssembly(typeof(UploadPhotoCommand)))
                .ConfigureServices()
                .ConfigureInfrastrucure(redisConfiguration);

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseCors();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}