using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PhotoUploader.Api.Models;
using PhotoUploader.ApplicationLayer.Services.CommandHandlers;

namespace PhotoUploader.Api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ImageUploadController : ControllerBase
    {
        private readonly ILogger<ImageUploadController> _logger;

        private readonly IMapper _mapper;

        private readonly IMediator _mediator;

        public ImageUploadController(ILogger<ImageUploadController> logger, IMapper mapper, IMediator mediator)
        {
            _logger = logger;
            _mapper = mapper;
            _mediator = mediator;
        }

        [HttpPost(Name = "UploadImage")]
        public async Task<IActionResult> Upload([FromForm] UploadImageRequest uploadImageRequest)
        {
            UploadPhotoCommand uploadCommand = _mapper.Map<UploadImageRequest, UploadPhotoCommand>(uploadImageRequest);

            var result = await _mediator.Send(uploadCommand);

            return Accepted();
        }
    }
}