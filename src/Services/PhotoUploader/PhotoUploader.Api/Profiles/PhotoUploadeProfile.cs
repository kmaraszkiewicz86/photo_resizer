﻿using AutoMapper;
using PhotoUploader.Api.Models;
using PhotoUploader.ApplicationLayer.Services.CommandHandlers;

namespace PhotoUploader.Api.Profiles
{
    public class PhotoUploadeProfile : Profile
    {
        public PhotoUploadeProfile()
        {
            CreateMap<UploadImageRequest, UploadPhotoCommand>();
        }
    }
}
