﻿using FluentAssertions;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Moq;
using PhotoUploader.Api.Models;
using PhotoUploader.ApplicationLayer.Validators;

namespace PhotoUploader.AppliationLayer.Tests.Validators
{
    public class UploadImageRequestValidatorTests
    {
        private UploadImageRequestValidator _sut;

        private Mock<IFormFile> _fromFileMock = new Mock<IFormFile>();

        public UploadImageRequestValidatorTests()
        {
            _sut= new UploadImageRequestValidator();
        }

        [Test]
        public void When_ModelIsValid_ThenReturnValidResult()
        {
            //Arrange
            var model = new UploadImageRequest("test", _fromFileMock.Object, "test");

            //Act
            ValidationResult result = _sut.Validate(model);

            //Assert
            result.IsValid.Should().BeTrue();
            result.Errors.Should().BeNullOrEmpty();
        }

        [Test]
        public void When_ModelHasNullableValues_ThenReturnInvalidResult()
        {
            //Arrange
            var model = new UploadImageRequest(null, null, null);

            //Act
            ValidationResult result = _sut.Validate(model);

            //Assert
            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(3);
        }

        [Test]
        public void When_ModelHasEmptyName_ThenReturnInvalidResult()
        {
            //Arrange
            var model = new UploadImageRequest(string.Empty, _fromFileMock.Object, "test");

            //Act
            ValidationResult result = _sut.Validate(model);

            //Assert
            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
        }

        [Test]
        public void When_ModelHasEmptyDescription_ThenReturnInvalidResult()
        {
            //Arrange
            var model = new UploadImageRequest("test", _fromFileMock.Object, string.Empty);

            //Act
            ValidationResult result = _sut.Validate(model);

            //Assert
            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
        }

        [Test]
        public void When_ModelHasNullabeFile_ThenReturnInvalidResult()
        {
            //Arrange
            var model = new UploadImageRequest("test", null, "test");

            //Act
            ValidationResult result = _sut.Validate(model);

            //Assert
            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
        }
    }
}
