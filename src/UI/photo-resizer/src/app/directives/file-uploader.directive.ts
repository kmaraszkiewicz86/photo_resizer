import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Directive({
  selector: 'input[type=file]',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: FileUploaderDirective,
    multi: true
  }]
})
export class FileUploaderDirective implements ControlValueAccessor {

  onChange: Function = () => { };

  private file: File | null = null;

  @HostListener('change', ['$event.target.files']) emitFiles( event: FileList ) {
    const file = event && event.item(0)
    this.onChange(file)
    this.file = file;
  }

  constructor(private element: ElementRef, private render: Renderer2) { }

  writeValue(value: any) {
    this.element.nativeElement.value = value === null || value === undefined ? '' : value;
    this.render.setProperty(this.element.nativeElement, 'value', value);
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }
  
  setDisabledState?(isDisabled: boolean): void {
  }
}
