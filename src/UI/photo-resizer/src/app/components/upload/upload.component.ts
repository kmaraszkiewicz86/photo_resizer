import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Observable } from 'rxjs';

import { UploadImageRequest } from 'src/app/models/upload-image-request';
import { FileUploadRequestService } from 'src/app/services/file-upload-request.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html'
})
export class UploadComponent implements OnInit {

  errorMessage: string = '';

  file: File;

  uploadForm = new FormGroup({
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    uploadImage: new FormControl('', Validators.required)
  })

  constructor(private service: FileUploadRequestService) { }

  ngOnInit(): void {
    this.uploadForm.get('uploadImage')?.valueChanges.subscribe((selectedFile) => {
      this.file = selectedFile as unknown as File 
    })
  }

  submit() {

    const name = this.uploadForm.get("name")?.value ?? '';
    const description = this.uploadForm.get("description")?.value ?? '';

    const request = { name: name, uplaodedImage: this.file, description: description }

    this.service.upload(request).subscribe({
      next: (v) => console.log(v),
      error: (e) => console.error(e),
      complete: () => console.info('complete') 
    })
  }
}
