import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpRequest } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UploadImageRequest } from '../models/upload-image-request';

@Injectable({
  providedIn: 'root'
})
export class FileUploadRequestService {

  private url = "http://localhost:5252/api/v1/ImageUpload";

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private http: HttpClient) { }

  upload(uploadRequest: UploadImageRequest) {

    const formData = new FormData();
    formData.append("name", uploadRequest.name);
    formData.append("description", uploadRequest.description);
    formData.append("uplaodedImage", uploadRequest.uplaodedImage);

    return this.http.post(this.url, formData)
      .pipe(
        catchError(this.handleError)
      )
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new Error('Something bad happened; please try again later.'));
  }
}
